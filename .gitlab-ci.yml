---
include:
  - project: cki-project/cki-lib
    file: .gitlab/ci_templates/cki-common.yml
    ref: production
  - local: .gitlab/ci_templates/internal-runner.yml
    rules:  # use GitLab runners when running in fork
      - if: $CI_MERGE_REQUEST_ID == null || $CI_PROJECT_ID == $CI_MERGE_REQUEST_PROJECT_ID

test:
  extends: .cki_tools
  script:
    - pip install git+https://gitlab.com/cki-project/kpet.git@production
    - kpet case tree
    - |
      missing_set=$(comm -3 <(kpet test list --set '.*' | sort) <(kpet test list | sort))
      if [[ -n "${missing_set}" ]]; then
        echo "The following tests are missing set information"
        echo "${missing_set}"
        exit 1
      fi
    - |
      readarray -t trees < <(kpet tree list)
      for tree in "${trees[@]}"; do
          readarray -t arches < <(kpet arch list --tree $tree)
          for arch in "${arches[@]}"; do
            if ! kpet run generate -t $tree -a $arch 1>/dev/null; then
              echo "ERROR: couldn't generate job for tree $tree on arch $arch"
              exit 1
            fi
          done
      done
    - |
      readarray -t trees < <(kpet tree list)
      for tree in "${trees[@]}"; do
          readarray -t arches < <(kpet arch list --tree $tree)
          for arch in "${arches[@]}"; do
            readarray -t repo_urls < <(kpet run generate -t $tree -a $arch | (grep "repo name" || true) | sort | uniq | awk '{print$3}' | cut -d '"' -f 2)
            # exclude links that run `uname -i` as they don't work on ci...
            repo_urls+=($(kpet run generate -t $tree -a $arch | (grep "baseurl=" || true) | (grep -v "uname -i" || true) | cut -d '=' -f 2 | sort | uniq))
            [[ "${#repo_urls[@]}" -eq 0 ]] && continue
            for repo in "${repo_urls[@]}"; do
              echo "Checking $tree $arch $repo"
              if ! curl --config "${CKI_CURL_CONFIG_FILE}" "${repo}" &>/dev/null; then
                echo "ERROR: couldn't query repo $repo for tree $tree on arch $arch"
                exit 1
              fi
            done
          done
      done
    - kpet run generate -t rhel8 -a x86_64 -k kernel .ci/nvme.patch |
      grep fetch.*storage/blktests/blk
    # make sure the host_type_regex doesn't include panicky host_types
    - kpet run generate -t rhel8 -a x86_64 -k kernel --high-cost yes --set stor --tests '.*megaraid_sas.*' |
      grep 'host type' | grep -v panicky
    # Tests from upstream trees have origin 'suites_zip' or 'ofa_rdma_repo'
    - |
      private_tests=$(kpet test list -t 'upstream|rawhide|eln' -o json |
      jq -r '.[] | select(.origin != "suites_zip") | select(.origin != "ofa_rdma_repo") | .name')
      if [ -n "$private_tests" ]
      then
        echo "ERROR: The following private tests run for upstream trees:"
        echo "$private_tests"
      fi
      [ -z "$private_tests" ]
    # Make sure test maintainers have gitlab contact
    - |
      missing_gitlab=$(kpet test list -o json |
      jq -r '.[] | select(.maintainers[].gitlab == null) | .name + " " + (.maintainers|tostring)')
      if [ -n "$missing_gitlab" ]
      then
        echo "ERROR: The following tests have test maintainers without gitlab contact:"
        echo "$missing_gitlab"
      fi
      [ -z "$missing_gitlab" ]

lint:
  extends: .cki_tools
  script:
    - "find -name '*.xml' | sed -e 's/^/Unexpected .xml file found, should probably be .xml.j2: /' | { ! grep .; }"
    - find -name '*.yaml' -exec yamllint -s {} +
    # check if trigger regex are sane
    # handle cases like include/linux/(efi.h|efi-.*.h)
    # ignore lines with "ci:skip-regex-check" in a comment
    - |
      {
        grep -nr '^ *- .*[^\\]\.[a-z0-9(]' --exclude '\.gitlab-ci.yml' |
          grep -v '#.*ci:skip-regex-check' ||
          true
      } | sed -e '1s/^/Possible unescaped characters in regexes:\n/; $q1'

codeowners:
  extends: .cki_tools
  script:
    - |
      # compare CODEOWNERS
      export GITLAB_TOKENS='{"gitlab.com":"GITLAB_JOB_CODEOWNERS_KPET_DB_MR_ACCESS_TOKEN"}'
      python3 -m cki.deployment_tools.gitlab_codeowners_config \
          ${CI_MERGE_REQUEST_IID:+--comment-mr-iid "${CI_MERGE_REQUEST_IID}"} \
          diff

deploy-production:
  extends: .deploy_production_tag
